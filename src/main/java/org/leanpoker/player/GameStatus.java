package org.leanpoker.player;
/**
 * Created by Zoltan_Pozsonyi on 2016-07-29.
 */
public enum GameStatus {
    BET, FLOP, TURN, RIVER;

}
