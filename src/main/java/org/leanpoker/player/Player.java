package org.leanpoker.player;

import org.leanpoker.hand.CardCheckResultForColour;
import org.leanpoker.hand.CardCheckResultForCount;
import org.leanpoker.hand.CardChecker;
import org.leanpoker.model.GameState;

import com.google.gson.Gson;
import com.google.gson.JsonElement;

public class Player {

    static final String VERSION = "1.0.3";

    private static int count = 0;

    public static int betRequest(JsonElement request) {
        Gson gson = new Gson();
        GameState state = gson.fromJson(request, GameState.class);

        try {

            CardCheckResultForCount countResult = CardChecker.maxNumberOfSameCards(state.getAllCards());
            CardCheckResultForCount countInHand = CardChecker.maxNumberOfSameCards(state.getPlayers()
                    .get(state.getInAction()).getHoleCards());
            CardCheckResultForColour colourResult = CardChecker.checkColours(state.getAllCards());
            if (countResult.getCount() == 4) {
                return getBet(state) + state.getMinimumRaise() * 2 < 1000 ? 1000 : getBet(state)
                        + state.getMinimumRaise() * 2;
            }
            if (colourResult.getCount() == 5
                    || (colourResult.getCount() == 4 && state.getGameStatus() != GameStatus.RIVER)) {
                return 10000;
            }
            if (countResult.getCount() == 3) {
                return getBet(state) + state.getMinimumRaise() * 2;
            }
            if (countInHand.getCount() == 2 && countInHand.getRank() > 8) {
                return getBet(state) + state.getMinimumRaise();
            }
        } catch (Exception exc) {

        }

        count++;
        if (count < 10) {
            int increase = getBet(state);
            if (state.getActivePlayers() == 3) {
                return 0;
            }
            return increase;
        }
        return 0;
    }

    private static int getBet(GameState state) {
        int bet = state.getCurrentBuyIn() - state.getPlayers().get(state.getInAction()).getBet();
        return bet > state.getMe().getStack() * 0.3 ? 0 : bet;
    }
    public static void showdown(JsonElement game) {
        count = 0;
    }
}
