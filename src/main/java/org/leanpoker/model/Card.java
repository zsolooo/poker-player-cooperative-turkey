package org.leanpoker.model;

import javax.annotation.Generated;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Generated("org.jsonschema2pojo")
public class Card {

    @SerializedName("rank")
    @Expose
    private String rank;
    @SerializedName("suit")
    @Expose
    private String suit;

    public Card(String rank, String suit) {
        this.rank = rank;
        this.suit = suit;
    }

    public Card() {
    }
    /**
     * 
     * @return The rank
     */
    public String getRank() {
        return rank;
    }

    /**
     * 
     * @param rank
     *            The rank
     */
    public void setRank(String rank) {
        this.rank = rank;
    }

    /**
     * 
     * @return The suit
     */
    public String getSuit() {
        return suit;
    }

    /**
     * 
     * @param suit
     *            The suit
     */
    public void setSuit(String suit) {
        this.suit = suit;
    }

}
