package org.leanpoker.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.annotation.Generated;

import org.leanpoker.player.GameStatus;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Generated("org.jsonschema2pojo")
public class GameState {

    /**
     * 
     * (Required)
     * 
     */
    @SerializedName("tournament_id")
    @Expose
    private String tournamentId;
    /**
     * 
     * (Required)
     * 
     */
    @SerializedName("game_id")
    @Expose
    private String gameId;
    /**
     * 
     * (Required)
     * 
     */
    @SerializedName("round")
    @Expose
    private Integer round;
    /**
     * 
     * (Required)
     * 
     */
    @SerializedName("bet_index")
    @Expose
    private Integer betIndex;
    /**
     * 
     * (Required)
     * 
     */
    @SerializedName("small_blind")
    @Expose
    private Integer smallBlind;
    /**
     * 
     * (Required)
     * 
     */
    @SerializedName("current_buy_in")
    @Expose
    private Integer currentBuyIn;
    /**
     * 
     * (Required)
     * 
     */
    @SerializedName("pot")
    @Expose
    private Integer pot;
    /**
     * 
     * (Required)
     * 
     */
    @SerializedName("minimum_raise")
    @Expose
    private Integer minimumRaise;
    /**
     * 
     * (Required)
     * 
     */
    @SerializedName("dealer")
    @Expose
    private Integer dealer;
    /**
     * 
     * (Required)
     * 
     */
    @SerializedName("orbits")
    @Expose
    private Integer orbits;
    /**
     * 
     * (Required)
     * 
     */
    @SerializedName("in_action")
    @Expose
    private Integer inAction;
    /**
     * 
     * (Required)
     * 
     */
    @SerializedName("players")
    @Expose
    private List<Player> players = new ArrayList<Player>();
    /**
     * 
     * (Required)
     * 
     */
    @SerializedName("community_cards")
    @Expose
    private List<Card> communityCards = new ArrayList<>();

    /**
     * 
     * (Required)
     * 
     * @return The tournamentId
     */
    public String getTournamentId() {
        return tournamentId;
    }

    /**
     * 
     * (Required)
     * 
     * @param tournamentId
     *            The tournament_id
     */
    public void setTournamentId(String tournamentId) {
        this.tournamentId = tournamentId;
    }

    /**
     * 
     * (Required)
     * 
     * @return The gameId
     */
    public String getGameId() {
        return gameId;
    }

    /**
     * 
     * (Required)
     * 
     * @param gameId
     *            The game_id
     */
    public void setGameId(String gameId) {
        this.gameId = gameId;
    }

    /**
     * 
     * (Required)
     * 
     * @return The round
     */
    public Integer getRound() {
        return round;
    }

    /**
     * 
     * (Required)
     * 
     * @param round
     *            The round
     */
    public void setRound(Integer round) {
        this.round = round;
    }

    /**
     * 
     * (Required)
     * 
     * @return The betIndex
     */
    public Integer getBetIndex() {
        return betIndex;
    }

    /**
     * 
     * (Required)
     * 
     * @param betIndex
     *            The bet_index
     */
    public void setBetIndex(Integer betIndex) {
        this.betIndex = betIndex;
    }

    /**
     * 
     * (Required)
     * 
     * @return The smallBlind
     */
    public Integer getSmallBlind() {
        return smallBlind;
    }

    /**
     * 
     * (Required)
     * 
     * @param smallBlind
     *            The small_blind
     */
    public void setSmallBlind(Integer smallBlind) {
        this.smallBlind = smallBlind;
    }

    /**
     * 
     * (Required)
     * 
     * @return The currentBuyIn
     */
    public Integer getCurrentBuyIn() {
        return currentBuyIn;
    }

    /**
     * 
     * (Required)
     * 
     * @param currentBuyIn
     *            The current_buy_in
     */
    public void setCurrentBuyIn(Integer currentBuyIn) {
        this.currentBuyIn = currentBuyIn;
    }

    /**
     * 
     * (Required)
     * 
     * @return The pot
     */
    public Integer getPot() {
        return pot;
    }

    /**
     * 
     * (Required)
     * 
     * @param pot
     *            The pot
     */
    public void setPot(Integer pot) {
        this.pot = pot;
    }

    /**
     * 
     * (Required)
     * 
     * @return The minimumRaise
     */
    public Integer getMinimumRaise() {
        return minimumRaise;
    }

    /**
     * 
     * (Required)
     * 
     * @param minimumRaise
     *            The minimum_raise
     */
    public void setMinimumRaise(Integer minimumRaise) {
        this.minimumRaise = minimumRaise;
    }

    /**
     * 
     * (Required)
     * 
     * @return The dealer
     */
    public Integer getDealer() {
        return dealer;
    }

    /**
     * 
     * (Required)
     * 
     * @param dealer
     *            The dealer
     */
    public void setDealer(Integer dealer) {
        this.dealer = dealer;
    }

    /**
     * 
     * (Required)
     * 
     * @return The orbits
     */
    public Integer getOrbits() {
        return orbits;
    }

    /**
     * 
     * (Required)
     * 
     * @param orbits
     *            The orbits
     */
    public void setOrbits(Integer orbits) {
        this.orbits = orbits;
    }

    /**
     * 
     * (Required)
     * 
     * @return The inAction
     */
    public Integer getInAction() {
        return inAction;
    }

    /**
     * 
     * (Required)
     * 
     * @param inAction
     *            The in_action
     */
    public void setInAction(Integer inAction) {
        this.inAction = inAction;
    }

    /**
     * 
     * (Required)
     * 
     * @return The players
     */
    public List<Player> getPlayers() {
        return players;
    }

    /**
     * 
     * (Required)
     * 
     * @param players
     *            The players
     */
    public void setPlayers(List<Player> players) {
        this.players = players;
    }

    /**
     * 
     * (Required)
     * 
     * @return The communityCards
     */
    public List<Card> getCommunityCards() {
        return communityCards;
    }

    /**
     * 
     * (Required)
     * 
     * @param communityCards
     *            The community_cards
     */
    public void setCommunityCards(List<Card> communityCards) {
        this.communityCards = communityCards;
    }

    public List<Card> getAllCards() {
        List<Card> result = new ArrayList<>();
        for (Card card : getCommunityCards()) {
            result.add(card);
        }
        for (Card card : players.get(inAction).getHoleCards()) {
            result.add(card);
        }
        return result;

    }

    public GameStatus getGameStatus() {
        GameStatus result;
        int size = getCommunityCards().size();
        if (size == 0) {
            result = GameStatus.BET;
        } else if (size == 3) {
            result = GameStatus.FLOP;
        } else if (size == 4) {
            result = GameStatus.TURN;
        } else {
            result = GameStatus.RIVER;
        }
        return result;
    }

    public int getActivePlayers() {
        int counter = 0;
        for (org.leanpoker.model.Player player : players) {
            if ("active".equals(player.getStatus()) || "folded".equals(player.getStatus())) {
                counter++;
            }
        }
        return counter;
    }

    public Player getMe() {
        return players.get(inAction);
    }

}
