package org.leanpoker.hand;
/**
 * Created by Zoltan_Pozsonyi on 2016-07-29.
 */
public class CardCheckResultForColour {

    private String suite;
    private int count;

    public CardCheckResultForColour(String suite, int count) {
        this.suite = suite;
        this.count = count;
    }

    public String getSuite() {
        return suite;
    }
    public void setSuite(String suite) {
        this.suite = suite;
    }
    public int getCount() {
        return count;
    }
    public void setCount(int count) {
        this.count = count;
    }
}
