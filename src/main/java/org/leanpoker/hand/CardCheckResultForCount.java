package org.leanpoker.hand;
/**
 * Created by Zoltan_Pozsonyi on 2016-07-29.
 */
public class CardCheckResultForCount {

    private int rank;
    private int count;

    public CardCheckResultForCount(int rank, int count) {
        this.rank = rank;
        this.count = count;
    }

    public int getRank() {
        return rank;
    }
    public void setRank(int rank) {
        this.rank = rank;
    }
    public int getCount() {
        return count;
    }
    public void setCount(int count) {
        this.count = count;
    }
}
