package org.leanpoker.hand;
import java.util.HashMap;
import java.util.List;

import org.leanpoker.model.Card;

public class CardChecker {

    public static CardCheckResultForCount maxNumberOfSameCards(List<Card> cardList) {
        int[] cardCount = new int[13];
        for (Card card : cardList) {
            switch (card.getRank()) {
                case "2" :
                case "3" :
                case "4" :
                case "5" :
                case "6" :
                case "7" :
                case "8" :
                case "9" :
                case "10" :
                    cardCount[Integer.parseInt(card.getRank()) - 2]++;
                    break;
                case "J" :
                    cardCount[9]++;
                    break;
                case "Q" :
                    cardCount[10]++;
                    break;
                case "K" :
                    cardCount[11]++;
                    break;
                case "A" :
                    cardCount[12]++;
                    break;
            }
        }
        CardCheckResultForCount result = null;
        int max = 0;
        for (int i = 0; i < 13; i++) {
            if (cardCount[i] > max) {
                max = cardCount[i];
                result = new CardCheckResultForCount(i, max);
            }
        }
        return result;
    }

    public static CardCheckResultForColour checkColours(List<Card> cardList) {
        int[] colour = new int[4];
        for (Card card : cardList) {
            colour[convertColour(card.getSuit())]++;
        }
        int max = 0;
        String maxStr = "";
        for (int i = 0; i < colour.length; i++) {
            if (colour[i] > max) {
                max = i;
                maxStr = convertColour(i);
            }
        }
        return new CardCheckResultForColour(maxStr, max);
    }

    private static int convertColour(String suit) {
        switch (suit) {
            case "clubs" :
                return 0;
            case "spades" :
                return 1;
            case "hearts" :
                return 2;
            case "diamonds" :
                return 3;
            default :
                return -1;
        }
    }

    private static String convertColour(int i) {
        switch (i) {
            case 0 :
                return "clubs";
            case 1 :
                return "spades";
            case 2 :
                return "hearts";
            case 3 :
                return "diamonds";
            default :
                return "";
        }
    }

}
