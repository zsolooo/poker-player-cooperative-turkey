package org.leanpoker.hand;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.leanpoker.model.Card;

import junit.framework.Assert;
/**
 * Created by Zoltan_Pozsonyi on 2016-07-29.
 */
public class CardCheckerTest {

    @Test
    public void test() {
        List<Card> list = new ArrayList<>();
        list.add(new Card("K", "a"));
        list.add(new Card("Q", "a"));
        list.add(new Card("J", "a"));
        list.add(new Card("1", "a"));
        list.add(new Card("2", "a"));
        list.add(new Card("2", "a"));
        list.add(new Card("2", "a"));
        CardCheckResultForCount result = CardChecker.maxNumberOfSameCards(list);
        Assert.assertEquals(0, result.getRank());
        Assert.assertEquals(3, result.getCount());
    }

}
